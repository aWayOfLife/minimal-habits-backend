import { Router } from 'express';
import { createOrUpdateAccount, getAccountById } from '../controllers/account.controller.js';
import { validateSchema } from '../shared/helpers/methods/validate-schema.method.js';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { createOrUpdateAccountSchema } from '../shared/dtos/account.dto.js';
import { authMiddleware } from '../middlewares/auth.middleware.js';

const accountRouter: Router = Router();

accountRouter.get('/', authMiddleware, handleController(getAccountById));

accountRouter.post(
  '/',
  authMiddleware,
  validateSchema(createOrUpdateAccountSchema),
  handleController(createOrUpdateAccount),
);

export default accountRouter;
