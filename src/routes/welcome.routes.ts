import { Router } from 'express';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { getWelcome } from '../controllers/welcome.controller.js';

export const welcomeRouter: Router = Router();

welcomeRouter.get('/', handleController(getWelcome));
