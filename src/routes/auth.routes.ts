import { Router } from 'express';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { loginUser, logoutUser } from '../controllers/auth.controller.js';
import { authMiddleware } from '../middlewares/auth.middleware.js';

const authRouter: Router = Router();

authRouter.post('/login', handleController(loginUser));

authRouter.post('/logout', authMiddleware, handleController(logoutUser));

export default authRouter;
