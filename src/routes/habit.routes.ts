import { Router } from 'express';
import {
  createOrUpdateHabit,
  getAllHabitsForAccount,
  getHabitById,
  removeHabitById,
} from '../controllers/habit.controller.js';
import { validateSchema } from '../shared/helpers/methods/validate-schema.method.js';
import { createOrUpdateHabitSchema } from '../shared/dtos/habit.dto.js';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { authMiddleware } from '../middlewares/auth.middleware.js';

const habitRouter: Router = Router();

habitRouter.get('/', authMiddleware, handleController(getAllHabitsForAccount));
habitRouter.get('/habitById/:habitId', authMiddleware, handleController(getHabitById));

habitRouter.post(
  '/',
  authMiddleware,
  validateSchema(createOrUpdateHabitSchema),
  handleController(createOrUpdateHabit),
);

habitRouter.delete('/:habitId', authMiddleware, handleController(removeHabitById));

export default habitRouter;
