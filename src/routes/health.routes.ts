import { Router } from 'express';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { getHealth } from '../controllers/health.controller.js';

export const healthRouter: Router = Router();

healthRouter.get('/', handleController(getHealth));
