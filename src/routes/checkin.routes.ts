import { Router } from 'express';
import {
  createOrUpdateCheckin,
  getAllCheckinsForHabit,
  getCheckinById,
} from '../controllers/checkin.controller.js';
import { validateSchema } from '../shared/helpers/methods/validate-schema.method.js';
import { createOrUpdateCheckinSchema } from '../shared/dtos/checkin.dto.js';
import { handleController } from '../shared/helpers/methods/handle-controller.method.js';
import { authMiddleware } from '../middlewares/auth.middleware.js';

const checkinRouter: Router = Router();

checkinRouter.get('/:habitId', authMiddleware, handleController(getAllCheckinsForHabit));
checkinRouter.get('/checkinById/:checkinId', authMiddleware, handleController(getCheckinById));

checkinRouter.post(
  '/',
  authMiddleware,
  validateSchema(createOrUpdateCheckinSchema),
  handleController(createOrUpdateCheckin),
);

export default checkinRouter;
