export enum HttpStatusCode {
  OK = 200,
  CREATED = 201,
  ACCEPTED = 202,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
}

export enum HttpStatusCodeString {
  OK = 'Ok',
  CREATED = 'Created',
  ACCEPTED = 'Accepted',
  BAD_REQUEST = 'Bad Request',
  UNAUTHORIZED = 'Unauthozired',
  FORBIDDEN = 'Forbidden',
  NOT_FOUND = 'Not Found',
  INTERNAL_SERVER_ERROR = 'Internal Server Error',
}
