import { z } from 'zod';
export interface IAccount {
  accountId: string;
  name: string;
  email: string;
}

export class AccountDto implements IAccount {
  accountId: string;
  name: string;
  email: string;
}

export const accountSchema = z.object({
  accountId: z.string(),
  name: z.string(),
  email: z.string().nonempty().email(),
});

export const createOrUpdateAccountSchema = accountSchema.omit({ accountId: true });
