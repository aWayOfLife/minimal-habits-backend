import { z } from 'zod';

export interface ICheckin {
  habitId: string;
  checkinId: string;
  content: string;
  checkInDateTime: Date;
}

export class CheckinDto implements ICheckin {
  habitId: string;
  checkinId: string;
  content: string;
  checkInDateTime: Date;
}

export const checkinSchema = z.object({
  habitId: z.string(),
  checkinId: z.string(),
  content: z.string().nonempty().min(16).max(140),
  checkInDateTime: z.string().nonempty().datetime(),
});

export const createOrUpdateCheckinSchema = checkinSchema.omit({
  checkinId: true,
  checkInDateTime: true,
});
