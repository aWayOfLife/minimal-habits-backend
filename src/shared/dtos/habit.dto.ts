import { z } from 'zod';
import { ICheckin } from './checkin.dto.js';

export interface IHabit {
  habitId: string;
  accountId: string;
  title: string;
  commitment: number;
}

export class HabitDto implements IHabit {
  habitId: string;
  accountId: string;
  title: string;
  commitment: number;
}

export const habitSchema = z.object({
  habitId: z.string(),
  accountId: z.string(),
  title: z.string().nonempty(),
  commitment: z.number().min(1).max(7),
});

export const createOrUpdateHabitSchema = habitSchema.omit({ habitId: true, accountId: true });

export interface IStatHabit extends IHabit {
  checkinsThisWeek: number;
}

export interface IHabitCheckin extends IHabit {
  checkins: ICheckin[];
}
