import { createCustomLogger } from '../logger/index.js';
import { isOperationalError } from './common.methods.js';

const logger = createCustomLogger('register-error-handler.ts');

export const registerErrorHandler = () => {
  process.on('unhandledRejection', (error: Error) => {
    throw error;
  });
  process.on('uncaughtException', (error: Error) => {
    logger.error('Server Error', error.message);
    if (!isOperationalError(error)) {
      process.exit(1);
    }
  });
};
