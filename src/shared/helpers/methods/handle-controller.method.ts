import { Request, Response, NextFunction } from 'express';
import { createCustomLogger } from '../logger/index.js';
import { Controller } from '../../types/controller.type.js';

const logger = createCustomLogger('handle-controller.method.ts');

export const handleController =
  (controller: Controller) => async (request: Request, response: Response, next: NextFunction) => {
    try {
      await controller(request, response, next);
    } catch (error) {
      logger.error(error.message);
      next(error);
    }
  };
