import { NextFunction, Request, Response } from 'express';
import { ZodError, ZodSchema } from 'zod';
import { HTTP400Error } from '../error/api.error.js';

export const validateSchema = (schema: ZodSchema) => {
  return (request: Request, response: Response, next: NextFunction) => {
    try {
      schema.parse(request.body);
      next();
    } catch (error) {
      if (error instanceof ZodError) {
        const { errors } = error;
        const errorsList = errors?.map((error) => ({
          message: error.message,
          property: error.path[0],
        }));
        throw new HTTP400Error(JSON.stringify(errorsList));
      }
      next(error);
    }
  };
};
