import { HttpStatusCode, HttpStatusCodeString } from '../../enums/error.enum.js';
import { BaseError } from './base.error.js';

export class HTTP500Error extends BaseError {
  constructor(description = 'internal server error') {
    const name = HttpStatusCodeString.INTERNAL_SERVER_ERROR;
    const httpCode = HttpStatusCode.INTERNAL_SERVER_ERROR;
    const isOperational = true;
    super(name, httpCode, description, isOperational);
  }
}

export class HTTP400Error extends BaseError {
  constructor(description = 'bad request') {
    const name = HttpStatusCodeString.BAD_REQUEST;
    const httpCode = HttpStatusCode.BAD_REQUEST;
    const isOperational = true;
    super(name, httpCode, description, isOperational);
  }
}

export class HTTP401Error extends BaseError {
  constructor(description = 'unauthorized') {
    const name = HttpStatusCodeString.UNAUTHORIZED;
    const httpCode = HttpStatusCode.UNAUTHORIZED;
    const isOperational = true;
    super(name, httpCode, description, isOperational);
  }
}

export class HTTP403Error extends BaseError {
  constructor(description = 'forbidden') {
    const name = HttpStatusCodeString.FORBIDDEN;
    const httpCode = HttpStatusCode.FORBIDDEN;
    const isOperational = true;
    super(name, httpCode, description, isOperational);
  }
}

export class HTTP404Error extends BaseError {
  constructor(description = 'not found') {
    const name = HttpStatusCodeString.NOT_FOUND;
    const httpCode = HttpStatusCode.NOT_FOUND;
    const isOperational = true;
    super(name, httpCode, description, isOperational);
  }
}
