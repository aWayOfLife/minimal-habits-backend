import winston, { format } from 'winston';
import { logFormat } from '../format/format.logger.js';

const {
  format: { combine, timestamp, colorize },
} = winston;

export const productionLogger = (fileName: string) => {
  return winston.createLogger({
    level: 'info',
    format: combine(
      colorize(),
      timestamp({ format: 'HH:mm:ss' }),
      format((info) => ({ ...info, fileName }))(),
      logFormat,
    ),
    transports: [new winston.transports.Console()],
  });
};
