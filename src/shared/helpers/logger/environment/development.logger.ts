import winston, { format } from 'winston';
import { logFormat } from '../format/format.logger.js';

const {
  format: { combine, timestamp, colorize },
} = winston;

export const developmentLogger = (fileName: string) => {
  return winston.createLogger({
    level: 'debug',
    format: combine(
      colorize(),
      timestamp({ format: 'HH:mm:ss' }),
      format((info) => ({ ...info, fileName }))(),
      logFormat,
    ),
    transports: [new winston.transports.Console()],
  });
};
