import { developmentLogger } from './environment/development.logger.js';
import { productionLogger } from './environment/production.logger.js';

export const createCustomLogger = (fileName: string) => {
  if (process.env.NODE_ENV === 'production') {
    return productionLogger(fileName);
  }

  if (process.env.NODE_ENV === 'development') {
    return developmentLogger(fileName);
  }
};
