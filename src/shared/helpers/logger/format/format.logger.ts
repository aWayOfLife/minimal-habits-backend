import winston from 'winston';

const {
  format: { printf },
} = winston;

export const logFormat = printf(({ level, message, timestamp, fileName }) => {
  return `${timestamp} [${level}] [${fileName}] ${message}`;
});
