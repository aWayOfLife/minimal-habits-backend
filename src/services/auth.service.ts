import admin from '../firebase/index.js';
import { HTTP401Error } from '../shared/helpers/error/api.error.js';

export const loginUserAsync = async (idToken: string) => {
  const expiresIn = 60 * 60 * 24 * 1 * 1000;

  try {
    const sessionCookie = await admin.auth().createSessionCookie(idToken, { expiresIn });
    return { sessionCookie, expiresIn };
  } catch (error) {
    throw new HTTP401Error('Unauthorized');
  }
};

export const logoutUserAsync = async (sub: string) => {
  await admin.auth().revokeRefreshTokens(sub);
  return 'logout';
};
