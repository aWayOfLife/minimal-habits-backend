import prisma from '../prisma/client.js';
import { v4 as uuidv4 } from 'uuid';
import { getAccountByIdAsync } from './account.service.js';
import { HTTP400Error, HTTP404Error } from '../shared/helpers/error/api.error.js';
import { HabitDto } from '../shared/dtos/habit.dto.js';

export const getAllHabitsForAccountAsync = async (accountId: string) => {
  const habits =
    accountId &&
    (await prisma.habit.findMany({
      where: {
        account: {
          accountId,
        },
      },
      include: {
        checkins: true,
      },
    }));
  return habits ?? [];
};

export const getHabitByIdAsync = async (habitId: string, isThrowException = false) => {
  const existingHabit = habitId && (await prisma.habit.findFirst({ where: { habitId } }));
  if (isThrowException) {
    if (!existingHabit) {
      throw new HTTP404Error(`Habit does not exist for user with habitId ${habitId}`);
    }
  }
  return existingHabit;
};

export const createOrUpdateHabitAsync = async (accountId: string, habitDto: HabitDto) => {
  const account = accountId && (await getAccountByIdAsync(accountId));
  if (!account) {
    throw new HTTP400Error(
      `Cannot create habit for an account that does not exist - accountId ${accountId}`,
    );
  }
  if (!habitDto.habitId) {
    habitDto = {
      ...habitDto,
      accountId,
      habitId: uuidv4(),
    };
  }
  const createdOrUpdatedHabit = await prisma.habit.upsert({
    where: {
      habitId: habitDto.habitId,
    },
    update: {
      title: habitDto.title,
      commitment: habitDto.commitment,
    },
    create: {
      ...habitDto,
    },
  });

  return createdOrUpdatedHabit;
};

export const removeHabitByIdAsync = async (habitId: string) => {
  const existingHabit = habitId && (await getHabitByIdAsync(habitId));
  if (!existingHabit) {
    throw new HTTP400Error(`Habit does not exist for user with habitId ${habitId}`);
  }
  const deletedHabit = await prisma.habit.delete({ where: { habitId } });
  return deletedHabit;
};
