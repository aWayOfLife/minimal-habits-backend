import prisma from '../prisma/client.js';
import { v4 as uuidv4 } from 'uuid';
import { HTTP404Error } from '../shared/helpers/error/api.error.js';
import { AccountDto } from '../shared/dtos/account.dto.js';

export const getAccountByIdAsync = async (accountId: string, isThrowException = false) => {
  const existingAccount = accountId && (await prisma.account.findFirst({ where: { accountId } }));
  if (isThrowException) {
    if (!existingAccount) {
      throw new HTTP404Error(`Account does not exist for accountId ${accountId}`);
    }
  }
  return existingAccount;
};

export const getAccountByEmailAsync = async (email: string, isThrowException = false) => {
  const existingAccount = email && (await prisma.account.findFirst({ where: { email } }));
  if (isThrowException) {
    if (!existingAccount) {
      throw new HTTP404Error(`Account does not exist for email ${email}`);
    }
  }
  return existingAccount;
};

export const createOrUpdateAccountAsync = async (accountDto: AccountDto) => {
  if (!accountDto.accountId) {
    const existingAccount = await getAccountByEmailAsync(accountDto.email);
    accountDto = {
      ...accountDto,
      accountId: existingAccount.accountId ?? uuidv4(),
    };
  }
  const createdOrUpdatedAccount = await prisma.account.upsert({
    where: {
      accountId: accountDto.accountId,
    },
    update: {
      name: accountDto.name,
    },
    create: {
      ...accountDto,
    },
  });
  return createdOrUpdatedAccount;
};
