import prisma from '../prisma/client.js';
import { v4 as uuidv4 } from 'uuid';
import { getHabitByIdAsync } from './habit.service.js';
import { HTTP400Error, HTTP404Error } from '../shared/helpers/error/api.error.js';
import { CheckinDto } from '../shared/dtos/checkin.dto.js';

export const getAllCheckinsForHabitAsync = async (habitId: string) => {
  const checkins =
    habitId &&
    (await prisma.checkin.findMany({
      where: {
        habit: {
          is: {
            habitId,
          },
        },
      },
    }));
  return checkins ?? [];
};

export const getCheckinByIdAsync = async (checkinId: string, isThrowException = false) => {
  const existingCheckin = checkinId && (await prisma.checkin.findFirst({ where: { checkinId } }));
  if (isThrowException) {
    if (!existingCheckin) {
      throw new HTTP404Error(`Checkin does not exist for user with checkinId ${checkinId}`);
    }
  }
  return existingCheckin;
};

export const createOrUpdateCheckinAsync = async (checkinDto: CheckinDto) => {
  const { habitId } = checkinDto;
  const habit = habitId && (await getHabitByIdAsync(habitId));
  if (!habit) {
    throw new HTTP400Error(
      `Cannot create/update checkin for a habit that does not exist - habitId ${habitId}`,
    );
  }
  if (!checkinDto.checkinId) {
    checkinDto = {
      ...checkinDto,
      checkinId: uuidv4(),
    };
  }
  const createdOrUpdatedCheckin = await prisma.checkin.upsert({
    where: {
      checkinId: checkinDto.checkinId,
    },
    update: {
      content: checkinDto.content,
    },
    create: {
      ...checkinDto,
    },
  });
  return createdOrUpdatedCheckin;
};
