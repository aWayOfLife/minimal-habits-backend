import { NextFunction, Request, Response } from 'express';
import { createCustomLogger } from '../shared/helpers/logger/index.js';

const logger = createCustomLogger('logging.middleware.ts');

export const loggingMiddleware = (request: Request, response: Response, next: NextFunction) => {
  const start = Date.now();
  const originalSend = response.send;

  response.send = (...args) => {
    const end = Date.now();
    const elapsed = end - start;

    const logMetadata = {
      method: request.method,
      url: request.url,
      status: response.statusCode,
      responseTime: elapsed + 'ms',
    };

    if (response.statusCode < 400) {
      logger.info('Request completed', { ...logMetadata });
      logger.debug('Response sent', { ...logMetadata });
    }

    return originalSend.apply(response, args);
  };

  logger.info('Request received', {
    method: request.method,
    url: request.url,
    ip: request.ip,
  });
  logger.debug('Request processing', {
    method: request.method,
    url: request.url,
    body: request.body,
    ip: request.ip,
  });

  next();
};
