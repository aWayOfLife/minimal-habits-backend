import { NextFunction, Request, Response } from 'express';
import { isJSONString } from '../shared/helpers/methods/common.methods.js';
import { BaseError } from '../shared/helpers/error/base.error.js';
import { HTTP400Error } from '../shared/helpers/error/api.error.js';
import { createCustomLogger } from '../shared/helpers/logger/index.js';

const logger = createCustomLogger('error.middleware.ts');

export const errorMiddleware = async (
  err: BaseError,
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  if (err instanceof HTTP400Error && isJSONString(err.message)) {
    response.status(err.httpCode).json(JSON.parse(err.message));
  } else {
    response.status(err.httpCode || 500).send(err.message);
  }
  logger.error('Response error', err.message);
  next();
};
