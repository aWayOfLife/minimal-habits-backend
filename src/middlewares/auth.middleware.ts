import { HTTP401Error } from '../shared/helpers/error/api.error.js';
import { createCustomLogger } from '../shared/helpers/logger/index.js';
import { Request, Response, NextFunction } from 'express';
import admin from '../firebase/index.js';

const logger = createCustomLogger('auth.middleware.ts');

export const authMiddleware = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const sessionCookie = request.cookies?.session || '';
    const decodedClaims = await admin.auth().verifySessionCookie(sessionCookie, true);
    request.accountId = decodedClaims.uid;
    request.sub = decodedClaims.sub;
    next();
  } catch (error) {
    logger.error('Error verifying Firebase authentication');
    try {
      throw new HTTP401Error(error);
    } catch (error) {
      next(error);
    }
  }
};
