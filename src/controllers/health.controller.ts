import { Request, Response } from 'express';
import { getHealthAsync } from '../services/health.service.js';

export const getHealth = async (request: Request, response: Response) => {
  const healh = await getHealthAsync();
  response.json(healh);
};
