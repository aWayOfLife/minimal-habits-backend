import { Request, Response } from 'express';
import { loginUserAsync, logoutUserAsync } from '../services/auth.service.js';

export const loginUser = async (request: Request, response: Response) => {
  const idToken = request.body.idToken.toString();
  const { sessionCookie, expiresIn } = await loginUserAsync(idToken);
  const options = { maxAge: expiresIn, httpOnly: true, secure: false };
  response.cookie('session', sessionCookie, options);
  response.json({ status: 'success' });
};

export const logoutUser = async (request: Request, response: Response) => {
  response.clearCookie('session');
  await logoutUserAsync(request.sub);
  response.json({ status: 'success' });
};
