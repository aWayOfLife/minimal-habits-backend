import { Request, Response } from 'express';
import {
  createOrUpdateCheckinAsync,
  getAllCheckinsForHabitAsync,
  getCheckinByIdAsync,
} from '../services/checkin.service.js';
import { CheckinDto } from '../shared/dtos/checkin.dto.js';
import { HttpStatusCode } from '../shared/enums/error.enum.js';

export const getAllCheckinsForHabit = async (request: Request, response: Response) => {
  const habitId = request.params.habitId as string;
  const checkins = await getAllCheckinsForHabitAsync(habitId);
  response.json(checkins);
};

export const getCheckinById = async (request: Request, response: Response) => {
  const checkinId = request.params.checkinId;
  const existingCheckin = await getCheckinByIdAsync(checkinId, true);
  response.json(existingCheckin);
};

export const createOrUpdateCheckin = async (request: Request, response: Response) => {
  const checkinDto = request.body as CheckinDto;
  const createdOrUpdatedCheckin = await createOrUpdateCheckinAsync(checkinDto);
  response.json(createdOrUpdatedCheckin).status(HttpStatusCode.OK);
};
