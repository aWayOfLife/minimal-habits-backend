import { Request, Response } from 'express';
import { createOrUpdateAccountAsync, getAccountByIdAsync } from '../services/account.service.js';
import { AccountDto } from '../shared/dtos/account.dto.js';
import { HttpStatusCode } from '../shared/enums/error.enum.js';

export const getAccountById = async (request: Request, response: Response) => {
  const accountId = request.accountId;
  const existingAccount = await getAccountByIdAsync(accountId, true);
  response.json(existingAccount);
};

export const createOrUpdateAccount = async (request: Request, response: Response) => {
  const accountDto = request.body as AccountDto;
  const createdOrUpdatedAccount = await createOrUpdateAccountAsync(accountDto);
  response.status(HttpStatusCode.OK).json(createdOrUpdatedAccount);
};
