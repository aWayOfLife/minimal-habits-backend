import { Request, Response } from 'express';
import {
  createOrUpdateHabitAsync,
  getAllHabitsForAccountAsync,
  getHabitByIdAsync,
  removeHabitByIdAsync,
} from '../services/habit.service.js';
import { HabitDto } from '../shared/dtos/habit.dto.js';
import { HttpStatusCode } from '../shared/enums/error.enum.js';

export const getAllHabitsForAccount = async (request: Request, response: Response) => {
  const accountId = request.accountId as string;
  const habits = await getAllHabitsForAccountAsync(accountId);
  response.json(habits);
};

export const getHabitById = async (request: Request, response: Response) => {
  const habitId = request.params.habitId;
  const existingHabit = await getHabitByIdAsync(habitId, true);
  response.json(existingHabit);
};

export const createOrUpdateHabit = async (request: Request, response: Response) => {
  const habitDto = request.body as HabitDto;
  const accountId = request.accountId as string;
  const createdOrUpdatedHabit = await createOrUpdateHabitAsync(accountId, habitDto);
  response.json(createdOrUpdatedHabit).status(HttpStatusCode.OK);
};

export const removeHabitById = async (request: Request, response: Response) => {
  const habitId = request.params.habitId;
  const deletedHabit = await removeHabitByIdAsync(habitId);
  response.json(deletedHabit);
};
