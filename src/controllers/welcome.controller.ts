import { Request, Response } from 'express';
import { getWelcomeAsync } from '../services/welcome.service.js';

export const getWelcome = async (request: Request, response: Response) => {
  const welcome = await getWelcomeAsync();
  response.json(welcome);
};
