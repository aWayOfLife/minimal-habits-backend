import { Express } from 'express';
import express from 'express';
import 'dotenv/config';
import habitRouter from './routes/habit.routes.js';
import checkinRouter from './routes/checkin.routes.js';
import accountRouter from './routes/account.routes.js';
import { errorMiddleware } from './middlewares/error.middleware.js';
import { registerErrorHandler } from './shared/helpers/methods/register-error-handler.method.js';
import { createCustomLogger } from './shared/helpers/logger/index.js';
import { loggingMiddleware } from './middlewares/logging.middleware.js';
import { healthRouter } from './routes/health.routes.js';
import { welcomeRouter } from './routes/welcome.routes.js';
import cors from 'cors';
import authRouter from './routes/auth.routes.js';
import cookieParser from 'cookie-parser';

const logger = createCustomLogger('index.ts');

const port: string = process.env.PORT;
const app: Express = express();

// app.set('trust proxy', 1);

const corsOptions = {
  origin: [
    'http://localhost:5173',
    'http://localhost:4200',
    'https://minimal-habits.vercel.app',
    'https://minimal-habits-app.firebaseapp.com',
  ],
  credentials: true,
};

// MIDDLEWARE
app.use(cors(corsOptions));
app.use(express.json());
app.use(cookieParser());
app.use(loggingMiddleware);

//ROUTES
app.use('/', welcomeRouter);
app.use('/api/auth', authRouter);
app.use('/api/health', healthRouter);
app.use('/api/account', accountRouter);
app.use('/api/habit', habitRouter);
app.use('/api/checkin', checkinRouter);

//MIDDLEWARE
app.use(errorMiddleware);

app.listen(port, () => {
  logger.info(`now listening on port ${port}`);
  registerErrorHandler();
});

export default app;
