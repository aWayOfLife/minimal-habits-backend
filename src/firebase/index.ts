import admin from 'firebase-admin';
import { getServiceAccount } from './configuration/service-account.config.js';

admin.initializeApp({
  credential: admin.credential.cert(getServiceAccount()),
});

export default admin;
