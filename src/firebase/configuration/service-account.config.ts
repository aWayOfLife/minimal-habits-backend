import { IServiceAccountConfig } from '../../shared/types/interfaces/service-account.interface.js';

export const getServiceAccount = () => {
  return {
    type: process.env.FB_TYPE,
    projectId: process.env.FB_PROJECT_ID,
    privateKeyId: process.env.FB_PRIVATE_KEY_ID.replace(/\\n/gm, '\n'),
    privateKey: process.env.FB_PRIVATE_KEY.replace(/\\n/gm, '\n'),
    clientEmail: process.env.FB_CLIENT_EMAIL,
    clientId: process.env.FB_CLIENT_ID,
    authUri: process.env.FB_AUTH_URI,
    tokenUri: process.env.FB_TOKEN_URI,
    authProviderX509CertUrl: process.env.FB_AUTH_PROVIDER_X509_CERT_URL,
    clientX509CertUrl: process.env.FB_CLIENT_X509_CERT_URL,
    firebaseAppUrl: process.env.FB_APP_URL,
  } as IServiceAccountConfig;
};
